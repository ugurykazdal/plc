package com.unisoft.plc.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.unisoft.plc.DataModel;
import com.unisoft.plc.R;


public class FireSensorAdapter extends RecyclerView.Adapter<FireSensorAdapter.MyViewHolder> {
    View mView;
    private List<DataModel> mDataset;

    public FireSensorAdapter(List<DataModel> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public FireSensorAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_fire_sensor_warning, parent, false);
        return new MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tv_row.setTextColor(mView.getResources().getColor(android.R.color.holo_blue_dark));
        holder.tv_row.setText(mDataset.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void scrollToposition() {

    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView tv_row;
        ImageView img_occGoReservation;

        MyViewHolder(View v) {
            super(v);
            tv_row = v.findViewById(R.id.tv_row);
        }
    }
}