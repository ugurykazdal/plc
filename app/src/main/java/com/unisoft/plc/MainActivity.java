//
//  MainActivity.java
//  MXComponentSample
//
//  Copyright (c) 2015 Mitsubishi Electric Corporation. All rights reserved.
//


package com.unisoft.plc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.unisoft.plc.MXComponentManager.MXComponentManagerCallback;
import com.unisoft.plc.adapter.DurusNedeniAdapter;
import com.unisoft.plc.adapter.FireSensorAdapter;
import com.unisoft.plc.adapter.UyarıAdapter;

import static br.com.zbra.androidlinq.Linq.stream;

public class MainActivity extends Activity implements MXComponentManagerCallback {

    public static int SEQ_READ_BOOL_1_1 = 11;
    public static int SEQ_READ_BOOL_1_1_SIZE = 960;
    public static int SEQ_READ_BOOL_1_6 = 16;
    public static int SEQ_READ_BOOL_1_6_SIZE = 960;
    public static int SEQ_READ_DWORD_1_1_3 = 113;
    public static int SEQ_READ_DWORD_1_1_3_SIZE = 52;

    boolean isWord = true;
    int isWordCount = 1;

    List<DataModel> mUyariList = new ArrayList<>();
    List<DataModel> mRawUyariList = new ArrayList<>();
    List<DataModel> mRawFireSensor = new ArrayList<>();
    List<DataModel> mFireSensorList = new ArrayList<>();
    List<DataModel> mRawDurusNedeni = new ArrayList<>();
    List<DataModel> mDurusNedeniList = new ArrayList<>();

    private ConstraintLayout mConsLyOperatorEkrani, mConsLyDurusNedeni;
    private RecyclerView mRecSensor, mRecUyari, mRecDurusNeden;
    private TextView mTv_actualOOE, mTv_actualFire, mTV_pad, mTv_calismaSure, mTvDurusNedenSayac, mTv_operatorName;
    private int current_sequence = 0;
    private MXComponentManager mxmanager;
    private CountDownTimer mCountDownTimerDurusSayac;
    private long mTimerValueForStopInSeconds = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_demo);

        mTv_operatorName = findViewById(R.id.activity_demo_operatorName);
        mTv_operatorName.setText(MyPreferences.GetOperatorName(this) + " OPERATÖR EKRANI");

        mTv_actualFire = findViewById(R.id.tv_actualFire);
        mTv_actualOOE = findViewById(R.id.tv_actualOee);
        mTv_calismaSure = findViewById(R.id.tv_calismaSure);
        mTV_pad = findViewById(R.id.tv_actualDpad);
        mTvDurusNedenSayac = findViewById(R.id.tv_durusNeden_sayac);

        mConsLyDurusNedeni = findViewById(R.id.consLy_durusNedeni);
        mConsLyOperatorEkrani = findViewById(R.id.consLy_operatorEkrani);

        mRecUyari = findViewById(R.id.rec_uyari);
        mRecDurusNeden = findViewById(R.id.rec_durusNeden);
        mRecSensor = findViewById(R.id.recList_fireSensorKamera);

        mRecUyari.setHasFixedSize(true);
        mRecUyari.setLayoutManager(new LinearLayoutManager(this));

        mRecDurusNeden.setHasFixedSize(true);
        mRecDurusNeden.setLayoutManager(new LinearLayoutManager(this));

        mRecSensor.setHasFixedSize(true);
        mRecSensor.setLayoutManager(new LinearLayoutManager(this));

        new JsonData().getData(this);
        if (JsonData.data != null) {
            mRawUyariList = JsonData.data.getWarning();
            mRawFireSensor = JsonData.data.getFireSensorKamera();
            mRawDurusNedeni = JsonData.data.getStopNedeni();

        }

        mxmanager = new MXComponentManager(this);
        mxmanager.setCallback(this);
        mxmanager.execOpen(current_sequence);

        CountDownTimer x = new CountDownTimer(Long.MAX_VALUE, 100) {
            public void onTick(long millisUntilFinished) {

                if (isWord) {
                    mxmanager.execReadDeviceBlock(SEQ_READ_DWORD_1_1_3);

                } else {
                    mxmanager.execReadDeviceBlock(SEQ_READ_BOOL_1_1);
                }
                isWord = !isWord;
            }


            public void onFinish() {
                start();// here, when your CountDownTimer has finished , we start it again :)
            }
        };
        x.start();


        mTv_operatorName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                return false;
            }
        });

    }

    private void createDurusSayac() {

        mCountDownTimerDurusSayac = new CountDownTimer(Long.MAX_VALUE, 1000) {
            public void onTick(long millisUntilFinished) {
                mTimerValueForStopInSeconds++;
                String sayacText = "";
                long stopSureDakika = mTimerValueForStopInSeconds / 60;
                long stopSureSaniye = mTimerValueForStopInSeconds - (stopSureDakika * 60);

                if (stopSureDakika > 0) {
                    sayacText = String.valueOf(stopSureDakika) + " dk \n" + String.valueOf(stopSureSaniye) + " sn";
                } else {
                    sayacText = String.valueOf(stopSureSaniye) + " sn";
                }

                mTvDurusNedenSayac.setText(sayacText);

            }


            public void onFinish() {
                start();
            }
        }.start();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mxmanager != null) {
            mxmanager.execClose(current_sequence);
        }
    }

    /**
     * Name           : resultMXComponent (Delegate)
     * Description    : Callback from MX Manager Class on API Relust
     *
     * @param seqno  : Update Record Sequence Number
     * @param result : API Result Code
     * @param time   : API Process Time (msec)
     * @param detail : ResultDetailString
     */
    public void resultMXComponent(int seqno, int result, long time, String detail, int[] data) {

        if (seqno == SEQ_READ_DWORD_1_1_3) {
            doEkranIstatistikWorks(data);

        }
        if (seqno == SEQ_READ_BOOL_1_1) {
            doRecyclerListWorks(data);
        }

    }

    private void doRecyclerListWorks(int[] data) {
        if (data == null) return;

        int address = 0;

        if (data.length > 0) {
            String binaryForStopCount = ConvertDecimalToBinary.getBinaryNumber(data[839]);
            if (binaryForStopCount.charAt(3) == '1') {
                mConsLyOperatorEkrani.setVisibility(View.INVISIBLE);
                mConsLyDurusNedeni.setVisibility(View.VISIBLE);
            } else {
                mConsLyDurusNedeni.setVisibility(View.INVISIBLE);
                mConsLyOperatorEkrani.setVisibility(View.VISIBLE);
                if (mCountDownTimerDurusSayac != null) {
                    mTimerValueForStopInSeconds = 0;
                    mCountDownTimerDurusSayac.cancel();
                    mCountDownTimerDurusSayac = null;

                }
            }
        }

        if (data.length > 0) {
            String binaryForStopCount = ConvertDecimalToBinary.getBinaryNumber(data[0]);
            if (binaryForStopCount.charAt(5) == '0') {
                if (mCountDownTimerDurusSayac == null) createDurusSayac();

            } else {
                if (mCountDownTimerDurusSayac != null) {
                    mTimerValueForStopInSeconds = 0;
                    mCountDownTimerDurusSayac.cancel();
                    mCountDownTimerDurusSayac = null;

                }
            }
        }

        for (int i = 0; i < data.length; i++) {
            String binary = ConvertDecimalToBinary.getBinaryNumber(data[i]);
            for (int m = 0; m < 16; m++) {
                if (binary.charAt(m) == '1') {
                    address = ((i + 1) * 16) - (m + 1);

                    for (int k1 = 0; k1 < mRawUyariList.size(); k1++) {
                        int finalAddress = address;

                        if (mRawUyariList.get(k1).getAddress().equals("M" + address)) {
                            if (!stream(mUyariList).where(w -> w.getAddress().equals("M" + finalAddress)).any()) {
                                mRawDurusNedeni.get(k1).setDescription(mRawDurusNedeni.get(k1).getDescription() + " / M" + finalAddress);
                                mUyariList.add(mRawUyariList.get(k1));
                                setUyariRecycler(mUyariList);
                            }
                        }
                    }

                    for (int k2 = 0; k2 < mRawFireSensor.size(); k2++) {
                        int finalAddressFire = address;
                        if (mRawFireSensor.get(k2).getAddress().equals("M" + address)) {
                            if (!stream(mFireSensorList).where(w -> w.getAddress().equals("M" + finalAddressFire)).any()) {
                                mRawDurusNedeni.get(k2).setDescription(mRawDurusNedeni.get(k2).getDescription() + " / M" + finalAddressFire);
                                mFireSensorList.add(mRawFireSensor.get(k2));
                                setFireSensorRecycler(mFireSensorList);
                            }
                        }
                    }

                    for (int k3 = 0; k3 < mRawDurusNedeni.size(); k3++) {
                        int finalAddressDurus = address;
                        if (mRawDurusNedeni.get(k3).getAddress().equals("M" + address)) {
                            if (!stream(mDurusNedeniList).where(w -> w.getAddress().equals("M" + finalAddressDurus)).any()) {
                                // String durusNedeniStr=mRawDurusNedeni.get(k3) +  " / M" + finalAddressDurus;
                                mRawDurusNedeni.get(k3).setDescription(mRawDurusNedeni.get(k3).getDescription() + " / M" + finalAddressDurus);
                                mDurusNedeniList.add(mRawDurusNedeni.get(k3));
                                setDurusNedeniRecycler(mDurusNedeniList);
                            }
                        }
                    }

                }
            }
        }

    }

    private void setUyariRecycler(List<DataModel> uyariList) {
        mRecUyari.setAdapter(new UyarıAdapter(uyariList));
    }

    private void setFireSensorRecycler(List<DataModel> fireSensorList) {
        mRecSensor.setAdapter(new FireSensorAdapter(fireSensorList));
    }

    private void setDurusNedeniRecycler(List<DataModel> durusNedenList) {
        mRecDurusNeden.setAdapter(new DurusNedeniAdapter(durusNedenList));
    }


    private void doEkranIstatistikWorks(int[] data) {

        int R4000_IYIURUN_KALAN = 0,
                R4000_IYIURUN_SHORT = 0,
                R4008_FIREADEDI_KALAN = 0,
                R4008_FIREADEDI_SHORT = 0,
                R4024_CALISMASURE_KALAN = 0,
                R4024_CALISMASURE_SHORT = 0,
                R4050_TOPLAMURETIM_KALAN = 0,
                R4050_TOPLAMURETIM_SHORT = 0;

        if (data != null) {

            try {
                R4000_IYIURUN_KALAN = data[0];
                R4000_IYIURUN_SHORT = data[1];

                R4008_FIREADEDI_KALAN = data[8];
                R4008_FIREADEDI_SHORT = data[9];

                R4024_CALISMASURE_KALAN = data[24];
                R4024_CALISMASURE_SHORT = data[25];

                R4050_TOPLAMURETIM_KALAN = data[2];//R4002
                R4050_TOPLAMURETIM_SHORT = data[3];

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        long toplamUretim = calculateDwordValue(R4050_TOPLAMURETIM_SHORT, R4050_TOPLAMURETIM_KALAN);
        long fireAdet = calculateDwordValue(R4008_FIREADEDI_SHORT, R4008_FIREADEDI_KALAN);
        long iyiUrunAdedi = calculateDwordValue(R4000_IYIURUN_SHORT, R4000_IYIURUN_KALAN);
        long calismaSure = calculateDwordValue(R4024_CALISMASURE_SHORT, R4024_CALISMASURE_KALAN);


        //--- CALCULATE & WRITE ACTUAL OEE
        long fark = toplamUretim - fireAdet;
        double farkOran = (double) fark / (double) toplamUretim;
        double finalVal = farkOran * 100;

        double actualOEE = (((double) (toplamUretim - fireAdet)) / (double) toplamUretim) * 100;

        String actualOeeStr = String.valueOf(actualOEE);
        if (actualOeeStr.length() > 4) {
            actualOeeStr = actualOeeStr.substring(0, 4);
        }
        mTv_actualOOE.setText(actualOeeStr + "%");

        //--- CALCULATE & WRITE ACTUAL FİRE
        double actualFire = (((double) fireAdet / (double) iyiUrunAdedi)) * 100;

        String actualFireStr = String.valueOf(actualFire);
        if (actualFireStr.length() > 3) {
            actualFireStr = actualFireStr.substring(0, 3);
        }

        mTv_actualFire.setText(actualFireStr + "%");

        //--- CALCULATE & WRITE PAD
        mTV_pad.setText(String.valueOf(fireAdet) + " Pad");

        //--- CALCULATE & WRITE CALISMA SURE
        long calismaSureSaat = calismaSure / 60;
        long calismaSureDakika = calismaSure - (calismaSureSaat * 60);
        mTv_calismaSure.setText(String.valueOf(calismaSureSaat) + " saat " + String.valueOf(calismaSureDakika) + " dk");
    }

    private long calculateDwordValue(int _shortVal, int _kalan) {
        return (_shortVal * 65536) + _kalan;
    }
}