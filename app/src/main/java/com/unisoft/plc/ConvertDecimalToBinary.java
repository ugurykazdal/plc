package com.unisoft.plc;

import java.util.Scanner;

//This program generates convert decimal to binary
public class ConvertDecimalToBinary {


    public static int getNumberOfBytes(int n) {
        int bytes = 0;

        if (n <= Byte.MIN_VALUE && n >= Byte.MAX_VALUE) {
            bytes = 1;
        } else if (n <= Short.MIN_VALUE && n >= Short.MAX_VALUE) {
            bytes = 2;
        } else if (n <= Integer.MIN_VALUE && n >= Integer.MAX_VALUE) {
            bytes = 4;
        }

        return bytes;
    }

    //Convert a positive decimal number to binary
    public static String convertPositiveNumberToBinary(int n, int bytes, boolean reverse) {
        int bits = 8 * bytes;
        StringBuilder sb = new StringBuilder(bits); //in-bits
        if (n == 0) {
            sb.append("0");
        } else {
            while (n > 0) {
                sb.append(n % 2);
                n >>= 1;  //aka n/2
            }
        }

        if (sb.length() < bits) {
            for (int i = sb.length(); i < bits; i++) {
                sb.append("0");

            }
        }
        if (reverse) {
            return sb.toString();
        } else {
            return sb.reverse().toString();
        }
    }

    public static String getBinaryNumber(int number) {

        int bytes = getNumberOfBytes(number);
        String binary;
        binary = convertPositiveNumberToBinary(number, 2, false);

        System.out.println(String.format("Binary representation of {%s} is {%s}", number, binary));

        return binary;
    }
}