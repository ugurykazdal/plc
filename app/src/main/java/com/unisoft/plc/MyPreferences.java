package com.unisoft.plc;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPreferences {

    private static final String MY_PREFERENCES = "MyPrefs";

    private static final String KEY_UNIT_TYPE = "unitType";
    private static final String KEY_CPU_TYPE = "cpuType";
    private static final String KEY_HOST_ADRESS = "hostAddress";
    private static final String KEY_PROTOCOL_TYPE = "protocolType";
    private static final String KEY_DESTINATION_PORT_NUMBER = "destinationPortNumber";
    private static final String KEY_OPERATOR_NAME = "operatorName";

    public static void SaveUnitType(Context _context, int _value) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_UNIT_TYPE, _value);
        editor.apply();
    }

    public static int GetUnitType(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_UNIT_TYPE, 0x001A);
    }

    public static void SaveCpuType(Context _context, int _value) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_CPU_TYPE, _value);
        editor.apply();
    }

    public static int GetCpuType(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_CPU_TYPE, 0x0093);
    }

    public static void SaveHostAddress(Context _context, String _Value) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_HOST_ADRESS, _Value);
        editor.apply();
    }

    public static String GetHostAddress(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getString(KEY_HOST_ADRESS, "192.168.1.1");
    }

    public static void SaveProtocolType(Context _context, int _value) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_PROTOCOL_TYPE, _value);
        editor.apply();
    }

    public static int GetProtocolType(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_PROTOCOL_TYPE, 0x0005);
    }

    public static void SaveDestinationPortNumber(Context _context, int _value) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_DESTINATION_PORT_NUMBER, _value);
        editor.apply();
    }

    public static int GetDestinationPortNumber(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getInt(KEY_DESTINATION_PORT_NUMBER, 5002);
    }

    public static void SaveOperatorName(Context _context, String _value) {
        SharedPreferences preferences = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_OPERATOR_NAME, _value);
        editor.apply();
    }

    public static String GetOperatorName(Context _context) {
        SharedPreferences prfs = _context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        return prfs.getString(KEY_OPERATOR_NAME, "D8");
    }
}