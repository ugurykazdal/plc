package com.unisoft.plc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import jp.co.melco.mxcomponent.MELMxCommunication;
import jp.co.melco.mxcomponent.MELMxOpenSettings;

public class MXComponentManager {

    //[Load Library]
    static {
        try {
            System.loadLibrary("MXComponent");
        } catch (UnsatisfiedLinkError e) {
            System.out.println("loadLibrary Errer");
        }
    }

    //[Command Execute Status Flag]
    public Boolean commandexecute = false;
    public Boolean commandexecuteBool = false;
    String detailstring;
    //[MX Component Communication Class]
    private MELMxCommunication mxcomm;
    //[MX Component OpenSetting Class]
    private MELMxOpenSettings mxopen;
    //[MXComponent result Callback Delegate Pointer]
    private MXComponentManagerCallback callback = null;
    private String mHostAddress;
    private int mDestinationPortNumber, mUnitType, mCpuType, mProtocolType;


    public MXComponentManager(Context _context) {

        //[initialize MX Component]
        mxcomm = new MELMxCommunication();
        mxopen = new MELMxOpenSettings();

        //[Call OpenSettingClass Initalize function]
        mHostAddress = MyPreferences.GetHostAddress(_context);
        mUnitType = MyPreferences.GetUnitType(_context);
        mCpuType = MyPreferences.GetCpuType(_context);
        mDestinationPortNumber = MyPreferences.GetDestinationPortNumber(_context);
        mProtocolType = MyPreferences.GetProtocolType(_context);
        setOpenSetting();

    }

    public void setCallback(MXComponentManagerCallback _callback) {

        callback = _callback;
    }

    /**
     * Name        : Open Setting Initalize
     * Description : Initialize MELMxOpenSetting Class Members
     */
    private void setOpenSetting_1_6() {
        mxopen.hostAddress = "192.168.1.6";
        //[Set MELSEC Port]
        mxopen.destinationPortNumber = 1389;
        //[Set MELSEC UnitTypeNumber (see User's Manual)]
        mxopen.unitType = 0x002C; //[ex. Unit QJ71E71]
        //[Set MELSEC CPUTypeNumber (see User's Manual)]
        mxopen.cpuType = 0x0091;  //[ex. Q20UDEHCPU]
        //[Set MELSEC I/ONumber (see User's Manual)]
        mxopen.protocolType = 0x0005;
        mxopen.timeOut = 10000;
        mxopen.ioNumber = 0x03FF;
    }

    private void setOpenSetting() {
        //[NOTE:Change below Value for User's Environment]

        //[Set MELSEC HostAddress (Hostname or IPv4String)]
        // mxopen.hostAddress = "192.168.1.1";
        mxopen.hostAddress = mHostAddress;
        //[Set MELSEC Port]
        // mxopen.destinationPortNumber = 5002;
        mxopen.destinationPortNumber = mDestinationPortNumber;
        //[Set MELSEC UnitTypeNumber (see User's Manual)]
        //mxopen.unitType = 0x001A; //[ex. Unit QJ71E71]
        mxopen.unitType = mUnitType; //[ex. Unit QJ71E71]
        //[Set MELSEC CPUTypeNumber (see User's Manual)]
        //   mxopen.cpuType = 0x0093;  //[ex. Q13UDEHCPU]
        mxopen.cpuType = mCpuType;  //[ex. Q13UDEHCPU]
        //[Set MELSEC RemotePassword (if needed)]
        //[Set MELSEC I/ONumber (see User's Manual)]
        // mxopen.protocolType = 0x0005;
        mxopen.protocolType = mProtocolType;
        mxopen.timeOut = 10000;
        mxopen.cpuTimeOut = 0;
        // mxopen.ioNumber=0x03E1;
        mxopen.ioNumber = 0x03FF;
    }


    /**
     * Name        : Open MELSEC CONNECTION
     * Description : Execute MX Component "Open" Function
     *
     * @param seqno: Execute sequence number (result callback with this number)
     */
    @SuppressLint("StaticFieldLeak")
    public void execOpen(int seqno) {

        //[check command exetcute state ]
        if (commandexecute) return;

        //[set command exetcute state]
        commandexecute = true;

        final int seq = seqno;

        //[Call MX "Open" by Background Thread]
        new AsyncTask<Void, Void, Integer>() {

            //[Set Process Start Time]
            long start = System.currentTimeMillis();

            @Override
            protected Integer doInBackground(Void... arg0) {

                //[call "Open" API]
                return mxcomm.open(mxopen, "");
            }

            @Override
            protected void onPostExecute(Integer result) {

                //[Check Pointer(not nul)]
                if (null != callback) {

                    //[set DetailResultString Host/port String]
                    detailstring = getOpenString();


                    //[Set Process Duration]
                    long interval = System.currentTimeMillis() - start;

                    //[Call result callback]
                    callback.resultMXComponent(seq, result, interval, detailstring, null);

                }

                //[set command exetcute state]
                commandexecute = false;
                return;

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    /**
     * Name        : Close MELSEC CONNECTION
     * Description : Execute MX Component "Close" Function
     *
     * @param seqno: Execute sequence number (result callback with this number)
     */
    @SuppressLint("StaticFieldLeak")
    public void execClose(int seqno) {

        //[check command exetcute state ]
        if (commandexecute) return;

        //[set command exetcute state]

        commandexecute = true;

        final int seq = seqno;

        //[Call MX "Close" by Background Thread]
        new AsyncTask<Void, Void, Integer>() {

            //[Set Process Start Time]
            long start = System.currentTimeMillis();

            @SuppressLint("StaticFieldLeak")
            @Override
            protected Integer doInBackground(Void... arg0) {

                //[call "Close" API]
                return mxcomm.close();
            }

            @Override
            protected void onPostExecute(Integer result) {

                //[Check Pointer(not nul)]
                if (null != callback) {

                    //[set DetailResultString blank]
                    detailstring = "";

                    //[Set Process Duration]
                    long interval = System.currentTimeMillis() - start;

                    //[Call result callback]
                    callback.resultMXComponent(seq, result, interval, detailstring, null);

                }

                //[set command exetcute state]
                commandexecute = false;
                return;

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    /**
     * Name        : Read Device Block from MELSEC
     * Description : Execute MX Component "Read Device Block" Function
     *
     * @param seqno: Execute sequence number (result callback with this number)
     */
    @SuppressLint("StaticFieldLeak")
    public void execReadDeviceBlock(int seqno) {

        //[check command exetcute state ]
        if (commandexecute) return;

        //[set command exetcute state]
        commandexecute = true;

        final int seq = seqno;

        //[Call MX "Read Device Block" by Background Thread]
        new AsyncTask<Void, Void, int[]>() {

            //[Set Process Start Time]
            @SuppressLint("StaticFieldLeak")
            long start = System.currentTimeMillis();

            @Override
            protected int[] doInBackground(Void... arg0) {
                int[] readdata = new int[1];
                String deviceAddress = "";
                int dataSize = 0;

                if (seq == MainActivity.SEQ_READ_DWORD_1_1_3) {
                    readdata = new int[MainActivity.SEQ_READ_DWORD_1_1_3_SIZE];
                    deviceAddress = "R4000";
                    dataSize = MainActivity.SEQ_READ_DWORD_1_1_3_SIZE;
                }

                if (seq == MainActivity.SEQ_READ_BOOL_1_1) {
                    readdata = new int[MainActivity.SEQ_READ_BOOL_1_1_SIZE];
                    deviceAddress = "M0";
                    dataSize = MainActivity.SEQ_READ_BOOL_1_1_SIZE;
                }
                //[call "Read Device Block" API]
                //[ex. Device:"D100" size:"1"]
                int result = mxcomm.readDeviceBlock(deviceAddress, dataSize, readdata);
                //[if API Success Set DetailResultString Readed data("fault" set blank)]
                if (result == 0)
                    detailstring = "DATA=【" + readdata[0] + "】";
                else
                    detailstring = "";

                return readdata;

            }

            @Override
            protected void onPostExecute(int[] data) {

                //[Check Pointer(not nul)]
                if (callback != null) {


                    //[Set Process Duration]
                    long interval = System.currentTimeMillis() - start;

                    //[Call result callback]
                    if (!detailstring.equals(""))
                        callback.resultMXComponent(seq, 0, interval, detailstring, data);

                }

                //[set command exetcute state]
                commandexecute = false;
                return;

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    /**
     * Name        : Get Connect Stringh from OpenSettings
     * Description : Make Conect Host and Port Description String
     *
     * @return : Make String
     */
    public String getOpenString() {

        return "Host=【" + mxopen.getHostAddress() + "】Port=【" + mxopen.getDestinationPortNumber() + "】IoNumber=【" + (Integer.toHexString(mxopen.getIoNumber())).toUpperCase() + "】";
    }

    //[Interface MXComponent result Callback]
    public interface MXComponentManagerCallback {
        void resultMXComponent(int seqno, int result, long time, String detail, int[] data);
    }
}
