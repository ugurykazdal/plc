package com.unisoft.plc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataModel {

@SerializedName("TagName")
@Expose
private String tagName;
@SerializedName("Address")
@Expose
private String address;
@SerializedName("DataType")
@Expose
private String dataType;
@SerializedName("RespectDataType")
@Expose
private String respectDataType;
@SerializedName("ClientAccess")
@Expose
private String clientAccess;
@SerializedName("ScanRate")
@Expose
private String scanRate;
@SerializedName("Description")
@Expose
private String description;
@SerializedName("Id")
@Expose
private String id;

public String getTagName() {
return tagName;
}

public void setTagName(String tagName) {
this.tagName = tagName;
}

public String getAddress() {
return address;
}

public void setAddress(String address) {
this.address = address;
}

public String getDataType() {
return dataType;
}

public void setDataType(String dataType) {
this.dataType = dataType;
}

public String getRespectDataType() {
return respectDataType;
}

public void setRespectDataType(String respectDataType) {
this.respectDataType = respectDataType;
}

public String getClientAccess() {
return clientAccess;
}

public void setClientAccess(String clientAccess) {
this.clientAccess = clientAccess;
}

public String getScanRate() {
return scanRate;
}

public void setScanRate(String scanRate) {
this.scanRate = scanRate;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

}