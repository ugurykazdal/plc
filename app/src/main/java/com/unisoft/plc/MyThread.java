package com.unisoft.plc;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;

public class MyThread {
    public static interface MyThreadCallback {
        public void resultMyThread(int seqno, int result, long time, String detail);
    }
    private MyThreadCallback callback;
    private long time=new Date().getTime();

    public MyThread(MyThreadCallback callback) {
        this.callback=callback;
    }

    public void run(final int seqNo) {
        //[Call MX "Read Device Block" by Background Thread]
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            new AsyncTask<Void, Void, String>() {

                //[Set Process Start Time]
                long start = System.currentTimeMillis();

                @Override
                protected String doInBackground(Void... arg0) {
                       String hostname = "time.nist.gov";
                    int port = 13;
                    StringBuilder data = new StringBuilder();

                    try (Socket socket = new Socket(hostname, port)) {

                        InputStream input = socket.getInputStream();
                        InputStreamReader reader = new InputStreamReader(input);

                        int character;

                        while ((character = reader.read()) != -1) {
                            data.append((char) character);
                        }

                        System.out.println(data);


                    } catch (UnknownHostException ex) {

                        System.out.println("Server not found: " + ex.getMessage());

                    } catch (IOException ex) {

                        System.out.println("I/O error: " + ex.getMessage());
                    }

                    Log.e("execute", String.valueOf(seqNo) + "/" + String.valueOf(time) + "--" + String.valueOf(seqNo + "detail"));

                    return data.toString();

                }

                @Override
                protected void onPostExecute(String result) {
                    callback.resultMyThread(seqNo,seqNo, time, result);

                    return;

                }
            }.execute();
        }
        // }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }
}
