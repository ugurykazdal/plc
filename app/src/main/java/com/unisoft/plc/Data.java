package com.unisoft.plc;

import java.util.List;

public class Data {
    private List<DataModel> StopNedeni;
    private List<DataModel> FireSensorKamera;
    private List<DataModel> Warning;

    public List<DataModel> getStopNedeni() {
        return StopNedeni;
    }

    public void setStopNedeni(List<DataModel> stopNedeni) {
        StopNedeni = stopNedeni;
    }

    public List<DataModel> getFireSensorKamera() {
        return FireSensorKamera;
    }

    public void setFireSensorKamera(List<DataModel> fireSensorKamera) {
        FireSensorKamera = fireSensorKamera;
    }

    public List<DataModel> getWarning() {
        return Warning;
    }

    public void setWarning(List<DataModel> warning) {
        Warning = warning;
    }
}