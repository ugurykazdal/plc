package com.unisoft.plc;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class SettingsActivity extends Activity {
    private EditText mEdtHostAddress, mEdtDestinationPortNumber, mEdtOperatorName, mEdtUnitType, mEdtCpuType, mEdtProtocolType;
    private Button mBtnUygula, mBtnVazgec;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_settings);

        mEdtHostAddress = findViewById(R.id.edt_activitySettings_hostAddress);
        mEdtDestinationPortNumber = findViewById(R.id.edt_activitySettings_destinationPortNo);
        mEdtOperatorName = findViewById(R.id.edt_activitySettings_operatorName);
        mEdtUnitType = findViewById(R.id.edt_activitySettings_unitType);
        mEdtCpuType = findViewById(R.id.edt_activitySettings_cpuType);
        mEdtProtocolType = findViewById(R.id.edt_activitySettings_protocolType);

        mBtnUygula = findViewById(R.id.btn_activitySettings_uygula);
        mBtnVazgec = findViewById(R.id.btn_activitySettings_vazgec);

        initializeValues();

        mBtnUygula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    MyPreferences.SaveHostAddress(SettingsActivity.this, mEdtHostAddress.getText().toString().trim());
                    MyPreferences.SaveDestinationPortNumber(SettingsActivity.this, Integer.parseInt(mEdtDestinationPortNumber.getText().toString().trim()));
                    MyPreferences.SaveOperatorName(SettingsActivity.this, mEdtOperatorName.getText().toString().trim());
                    MyPreferences.SaveUnitType(SettingsActivity.this, Integer.parseInt(mEdtUnitType.getText().toString().trim(),16));
                    MyPreferences.SaveCpuType(SettingsActivity.this, Integer.parseInt(mEdtCpuType.getText().toString().trim(),16));
                    MyPreferences.SaveProtocolType(SettingsActivity.this, Integer.parseInt(mEdtProtocolType.getText().toString().trim(),16));

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(SettingsActivity.this);
                    builder1.setMessage("Uygulama yeniden başlatılacak.");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "TAMAM",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    restart();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        mBtnVazgec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEdtHostAddress.setText("");
                mEdtUnitType.setText("");
                mEdtCpuType.setText("");
                mEdtDestinationPortNumber.setText("");
                mEdtProtocolType.setText("");
                mEdtOperatorName.setText("");
                initializeValues();
            }
        });
    }

    private void initializeValues() {
        mEdtHostAddress.setText(MyPreferences.GetHostAddress(this));
        mEdtUnitType.setText(String.valueOf(MyPreferences.GetUnitType(this)));
        mEdtCpuType.setText(String.valueOf(MyPreferences.GetCpuType(this)));
        mEdtDestinationPortNumber.setText(String.valueOf(MyPreferences.GetDestinationPortNumber(this)));
        mEdtProtocolType.setText(String.valueOf(MyPreferences.GetProtocolType(this)));
        mEdtOperatorName.setText(MyPreferences.GetOperatorName(this));

    }

    public void restart() {

        final Intent mStartActivity = new Intent(SettingsActivity.this, MainActivity.class);
        final int mPendingIntentId = 123456;
        final PendingIntent mPendingIntent = PendingIntent.getActivity(SettingsActivity.this, mPendingIntentId, mStartActivity,
                PendingIntent.FLAG_CANCEL_CURRENT);
        final AlarmManager mgr = (AlarmManager) SettingsActivity.this.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, mPendingIntent);
        this.finishAffinity(); //notice here
        Runtime.getRuntime().exit(0); //notice here
    }
}